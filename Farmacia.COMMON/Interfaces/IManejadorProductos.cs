﻿using System;
using Farmacia.COMMON.Entidades;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaces
{
    public interface IManejadorProductos : IManejadorGenerico<Productos>
    {
        List<Productos> ProductosPorNombre(string nombre);
    }
}
