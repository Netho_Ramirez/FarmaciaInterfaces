﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para AgregarCategorias.xaml
    /// </summary>
    public partial class AgregarCategorias : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorCategorias manejadorCategorias;
        accion accionCategorias;
        bool esNuevo;
        public AgregarCategorias()
        {
            InitializeComponent();
            manejadorCategorias = new ManejadorCategorias(new RepositorioDeCategorias());
            PonerBotonesCategoriasEnEdicion(false);
            LimpiarCamposDeCategorias();
            ActualizarTablaCategorias();
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregarCategoria.IsEnabled = habilitados;
            btnEliminarCategoria.IsEnabled = habilitados;
            btnEditarCategoria.IsEnabled = habilitados;
            btnGuardarCategoria.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreCategoria.Clear();
            txbNombreCategoria.IsEnabled = habilitadas;           
        }

        private void ActualizarTablaCategorias()
        {
            dtgCategorias.ItemsSource = null;
            dtgCategorias.ItemsSource = manejadorCategorias.Listar;
        }

        private void LimpiarCamposDeCategorias()
        {
            txbNombreCategoria.Clear();           
            txbCategoriasId.Text = "";            
        }

        private void PonerBotonesCategoriasEnEdicion(bool value)
        {
            btnCancelar.IsEnabled = value;
            btnEditarCategoria.IsEnabled = !value;
            btnEliminarCategoria.IsEnabled = !value;
            btnGuardarCategoria.IsEnabled = value;
            btnAgregarCategoria.IsEnabled = !value;
        }

        private void btnAgregarCategoria_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeCategorias();
            PonerBotonesCategoriasEnEdicion(true);
            accionCategorias = accion.Nuevo;
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditarCategoria_Click(object sender, RoutedEventArgs e)
        {
            Categorias emp = dtgCategorias.SelectedItem as Categorias;
            if (emp != null)
            {
                HabilitarCajas(true);
                txbCategoriasId.Text = emp.Id;
                txbNombreCategoria.Text = emp.NombreCategoria;          
                accionCategorias = accion.Editar;
                PonerBotonesCategoriasEnEdicion(true);
            }
        }

        private void btnEliminarCategoria_Click(object sender, RoutedEventArgs e)
        {
            Categorias emp = dtgCategorias.SelectedItem as Categorias;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar esta Categoria?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorCategorias.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Categoria eliminada con éxito", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaCategorias();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar la Categoria", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnGuardarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (accionCategorias == accion.Nuevo)
            {
                Categorias emp = new Categorias()
                {
                    NombreCategoria = txbNombreCategoria.Text,                    
                };
                if (manejadorCategorias.Agregar(emp))
                {
                    MessageBox.Show("Categoria agregada correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeCategorias();
                    ActualizarTablaCategorias();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesCategoriasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("La Categoria no pudo ser agregada", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Categorias emp = dtgCategorias.SelectedItem as Categorias;
                emp.NombreCategoria = txbNombreCategoria.Text;
                if (manejadorCategorias.Modificar(emp))
                {
                    MessageBox.Show("Categoria modificada correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeCategorias();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTablaCategorias();
                    PonerBotonesCategoriasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("La Categoria no pudo ser actualizada", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarBotones(true);
            HabilitarCajas(false);
            LimpiarCamposDeCategorias();
            PonerBotonesCategoriasEnEdicion(false);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
