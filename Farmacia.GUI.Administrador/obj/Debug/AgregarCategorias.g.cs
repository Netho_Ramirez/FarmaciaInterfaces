﻿#pragma checksum "..\..\AgregarCategorias.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "961C93DF3F0CCDFEBF3BA955191851F061901B94"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Farmacia.GUI.Administrador;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Farmacia.GUI.Administrador {
    
    
    /// <summary>
    /// AgregarCategorias
    /// </summary>
    public partial class AgregarCategorias : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbNombreCategoria;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbCategoriasId;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgCategorias;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAgregarCategoria;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEditarCategoria;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminarCategoria;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarCategoria;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\AgregarCategorias.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCerrar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Farmacia.GUI.Administrador;component/agregarcategorias.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AgregarCategorias.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txbNombreCategoria = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.txbCategoriasId = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.dtgCategorias = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.btnAgregarCategoria = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\AgregarCategorias.xaml"
            this.btnAgregarCategoria.Click += new System.Windows.RoutedEventHandler(this.btnAgregarCategoria_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnEditarCategoria = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\AgregarCategorias.xaml"
            this.btnEditarCategoria.Click += new System.Windows.RoutedEventHandler(this.btnEditarCategoria_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnEliminarCategoria = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\AgregarCategorias.xaml"
            this.btnEliminarCategoria.Click += new System.Windows.RoutedEventHandler(this.btnEliminarCategoria_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnGuardarCategoria = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\AgregarCategorias.xaml"
            this.btnGuardarCategoria.Click += new System.Windows.RoutedEventHandler(this.btnGuardarCategoria_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\AgregarCategorias.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnCerrar = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\AgregarCategorias.xaml"
            this.btnCerrar.Click += new System.Windows.RoutedEventHandler(this.btnCerrar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

