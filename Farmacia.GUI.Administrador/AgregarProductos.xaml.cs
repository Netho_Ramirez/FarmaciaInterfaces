﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using System;
using System.Windows;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para AgregarProductos.xaml
    /// </summary>
    public partial class AgregarProductos : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorProductos manejadorProductos;

        accion accionProductos;
        bool esNuevo;
        public AgregarProductos()
        {
            InitializeComponent();
            manejadorProductos = new ManejadorProductos(new RepositorioDeProductos());            
            PonerBotonesProductosEnEdicion(false);
            LimpiarCamposDeProductos();
            ActualizarTablaProductos();
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregarProductoo.IsEnabled = habilitados;
            btnEliminaProducto.IsEnabled = habilitados;
            btnEditarProductoo.IsEnabled = habilitados;
            btnGuardarProducto.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreProducto.Clear();
            txbNombreProducto.IsEnabled = habilitadas;
            txbDescripcionProducto.Clear();
            txbDescripcionProducto.IsEnabled = habilitadas;
            txbPrecioCompra.Clear();
            txbPrecioCompra.IsEnabled = habilitadas;
            txbPrecioVenta.Clear();
            txbPrecioVenta.IsEnabled = habilitadas;
            txbPresentacion.Clear();
            txbPresentacion.IsEnabled = habilitadas;
        }

        private void ActualizarTablaProductos()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductos.Listar;
        }

        private void LimpiarCamposDeProductos()
        {
            txbDescripcionProducto.Clear();
            txbNombreProducto.Clear();
            txbPrecioCompra.Clear();
            txbPrecioVenta.Clear();
            txbPresentacion.Clear();
            txbProductosId.Text = "";
        }

        private void PonerBotonesProductosEnEdicion(bool value)
        {
            btnCancelar.IsEnabled = value;
            btnEditarProductoo.IsEnabled = !value;
            btnEliminaProducto.IsEnabled = !value;
            btnGuardarProducto.IsEnabled = value;
            btnAgregarProductoo.IsEnabled = !value;
        }

        private void btnAgregarProductoo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            PonerBotonesProductosEnEdicion(true);
            accionProductos = accion.Nuevo;
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditarProductoo_Click(object sender, RoutedEventArgs e)
        {
            Productos emp = dtgProductos.SelectedItem as Productos;
            if (emp != null)
            {
                HabilitarCajas(true);
                txbProductosId.Text = emp.Id;
                txbPresentacion.Text = emp.Presentacion;
                txbPrecioVenta.Text = emp.PrecioVenta;
                txbPrecioCompra.Text = emp.PrecioCompra;
                txbNombreProducto.Text = emp.NombreProducto;
                txbDescripcionProducto.Text = emp.Descripcion;
                accionProductos = accion.Editar;
                PonerBotonesProductosEnEdicion(true);
            }
        }

        private void btnEliminaProducto_Click(object sender, RoutedEventArgs e)
        {
            Productos emp = dtgProductos.SelectedItem as Productos;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Producto?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorProductos.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Producto eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaProductos();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Producto", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnGuardarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (accionProductos == accion.Nuevo)
            {
                Productos emp = new Productos()
                {
                    NombreProducto = txbNombreProducto.Text,
                    Descripcion = txbDescripcionProducto.Text,
                    Presentacion = txbPresentacion.Text,
                    PrecioVenta = txbPrecioVenta.Text,
                    PrecioCompra = txbPrecioCompra.Text,
                };
                if (manejadorProductos.Agregar(emp))
                {
                    MessageBox.Show("Producto agregado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    ActualizarTablaProductos();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Producto no se pudo agregar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Productos emp = dtgProductos.SelectedItem as Productos;
                emp.NombreProducto = txbNombreProducto.Text;
                emp.Descripcion = txbDescripcionProducto.Text;
                emp.Presentacion = txbPresentacion.Text;
                emp.PrecioVenta = txbPrecioVenta.Text;
                emp.PrecioCompra = txbPrecioCompra.Text;
                if (manejadorProductos.Modificar(emp))
                {
                    MessageBox.Show("Producto modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTablaProductos();
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Producto no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
            LimpiarCamposDeProductos();
            PonerBotonesProductosEnEdicion(false);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }        
    }
}
