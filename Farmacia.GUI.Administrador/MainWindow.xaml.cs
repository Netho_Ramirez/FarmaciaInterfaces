﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAgregarEmpleados_Click(object sender, RoutedEventArgs e)
        {
            AgregarEmpleados agregaEmpleado = new AgregarEmpleados();
            agregaEmpleado.Show();
        }

        private void btnAgregarClientes_Click(object sender, RoutedEventArgs e)
        {
            AgregarClientes agregarClientes = new AgregarClientes();
            agregarClientes.Show();
        }

        private void btnAgregarCategorias_Click(object sender, RoutedEventArgs e)
        {
            AgregarCategorias agregarCategorias = new AgregarCategorias();
            agregarCategorias.Show();
        }

        private void btnAgregarProductos_Click(object sender, RoutedEventArgs e)
        {
            AgregarProductos agregarProductos = new AgregarProductos();
            agregarProductos.Show();
        }
    }
}
