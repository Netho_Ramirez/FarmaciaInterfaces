﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para AgregarClientes.xaml
    /// </summary>
    public partial class AgregarClientes : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorClientes manejadorClientes;
        accion accionClientes;
        bool esNuevo;
        public AgregarClientes()
        {
            InitializeComponent();
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());

            PonerBotonesClientesEnEdicion(false);
            LimpiarCamposDeClientes();
            ActualizarTablaClientes();
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombre.Clear();
            txbNombre.IsEnabled = habilitadas;
            txbApellidos.Clear();
            txbApellidos.IsEnabled = habilitadas;
            txbDireccion.Clear();
            txbDireccion.IsEnabled = habilitadas;
            txbRFC.Clear();
            txbRFC.IsEnabled = habilitadas;
            txbTelefono.Clear();
            txbTelefono.IsEnabled = habilitadas;
            txbCorreo.Clear();
            txbCorreo.IsEnabled = habilitadas;
        }

        private void ActualizarTablaClientes()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = manejadorClientes.Listar;
        }

        private void LimpiarCamposDeClientes()
        {
            txbApellidos.Clear();
            txbNombre.Clear();
            txbClientesId.Text = "";
            txbCorreo.Clear();
            txbDireccion.Clear();
            txbRFC.Clear();
            txbTelefono.Clear();
        }

        private void PonerBotonesClientesEnEdicion(bool value)
        {
            btnCancelar.IsEnabled = value;
            btnEditar.IsEnabled = !value;
            btnEliminar.IsEnabled = !value;
            btnGuardar.IsEnabled = value;
            btnAgregar.IsEnabled = !value;
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(true);
            accionClientes = accion.Nuevo;
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Clientes emp = dtgClientes.SelectedItem as Clientes;
            if (emp != null)
            {
                HabilitarCajas(true);
                txbClientesId.Text = emp.Id;
                txbApellidos.Text = emp.ApellidosCliente;
                txbCorreo.Text = emp.Correo;
                txbDireccion.Text = emp.Direccion;
                txbNombre.Text = emp.NombreCliente;
                txbRFC.Text = emp.RFC;
                txbTelefono.Text = emp.Telefono;
                accionClientes = accion.Editar;
                PonerBotonesClientesEnEdicion(true);
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Clientes emp = dtgClientes.SelectedItem as Clientes;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar a este Cliente?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorClientes.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Cliente eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaClientes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar a su Cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionClientes == accion.Nuevo)
            {
                Clientes emp = new Clientes()
                {
                    NombreCliente = txbNombre.Text,
                    ApellidosCliente = txbApellidos.Text,
                    Direccion = txbDireccion.Text,
                    Telefono = txbTelefono.Text,
                    Correo = txbCorreo.Text,
                    RFC = txbRFC.Text,
                };
                if (manejadorClientes.Agregar(emp))
                {
                    MessageBox.Show("Su Cliente ha sido agregado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();                    
                    ActualizarTablaClientes();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo agregar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Clientes emp = dtgClientes.SelectedItem as Clientes;
                emp.NombreCliente = txbNombre.Text;
                emp.ApellidosCliente = txbApellidos.Text;
                emp.Direccion = txbDireccion.Text;
                emp.Telefono = txbTelefono.Text;
                emp.Correo = txbCorreo.Text;
                emp.RFC = txbRFC.Text;
                if (manejadorClientes.Modificar(emp))
                {
                    MessageBox.Show("Cliente modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTablaClientes();
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Cliente no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(false);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
